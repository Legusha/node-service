import jwt from 'jsonwebtoken'

function isAuthenticate (req, res, next) {
  if (typeof req.body.token !== 'undefined' || typeof req.query.token !== 'undefined') {
    const token = req.body.token || req.query.token;
    jwt.verify(token, process.env.SECRET_TOKEN, { algorithm: 'HS256' }, (err, login) => {
      if (err) {
        res.status(401).json({error: 'invalid_token'});
        return
      }
      // if the JWT is valid, allow them to hit
      return next();
    });
    return
  }
  res.status(401).json({error: 'invalid_token'});
}
export default isAuthenticate
