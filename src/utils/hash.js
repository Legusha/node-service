import crypto from 'crypto'

const saltHashPassword = (userPassword) => {
  const salt = randomString(28);
  return sha512(userPassword, salt);
};
const validatePassword = (password, hashedPass, salt) => {
  const passwordData = sha512(password, salt);
  return passwordData.passwordHash === hashedPass
};

function randomString (length) {
  return crypto
    .randomBytes(Math.ceil(length/2))
    .toString('hex')
    .slice(0,length)
}
function sha512 (password, salt) {
  const hash = crypto.createHmac('sha512', salt);
  hash.update(password);
  const value = hash.digest('hex');
  return {
    salt:salt,
    passwordHash:value
  }
}
export { saltHashPassword, validatePassword }
