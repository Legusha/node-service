import Admin from './model';
import Leads from '../lead/model';
import { saltHashPassword, validatePassword } from '@/utils/hash';
import jwt from 'jsonwebtoken';

const login = {
  errorCredentials: {status: 401, message: 'invalid_credentials'},
  jwtExpiry: '1h',
  async post (req, res, next) {
    const { login, password } = req.body;
    try {
      const data = await Admin.findOne({ where: { login } });
      if (data == null) {
        next(this.errorCredentials);
      }
      if (!validatePassword(password, data.password, data.salt)) {
        next(this.errorCredentials);
      }
      let token = jwt.sign(
        { login: data.login },
        process.env.SECRET_TOKEN,
        { algorithm: 'HS256', expiresIn: this.jwtExpiry }
      );
      res.status(200).json({ token });
    } catch (e) {
      console.error(e);
      next(e);
    }
  }
};
const hash = {
  async post (req, res, next) {
    const { password } = req.body;
    try {
      res.status(200).json(saltHashPassword(password) || null);
    } catch (e) {
      console.error(e);
      next(e);
    }
  }
};
const leads = {
  limit: 5,
  async get (req, res, next) {
    const { page } = req.query;
    const pageNumber = parseInt(page, 10);
    try {
      if (typeof page === 'undefined' || isNaN(pageNumber)) {
        next({status: 400, message: 'invalid_page_number'})
      }
      let offset = (pageNumber * this.limit) - this.limit;
      offset = offset < 0 ? 0 : offset;
      const result = await Leads.findAndCountAll({
        offset,
        limit: this.limit
      });
      const remnant = result.count - (page * this.limit);
      result.remnant = remnant < 0 ? 0 : remnant;
      res.status(200).json(result);
    } catch (e) {
      console.error(e);
      next(e)
    }
  }
};
export { login, leads, hash }
