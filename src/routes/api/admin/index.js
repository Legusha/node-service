import { Router } from 'express'
import { login, leads, hash } from './controller'
import authenticate from '@/middleware/authenticate';

const router = Router();

router.post('/login', login.post.bind(login));
router.get('/hash', hash.post);
router.get('/leads', authenticate, leads.get.bind(leads));

export default router
