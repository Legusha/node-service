import Sequelize from 'sequelize'
class Admin extends Sequelize.Model {}
import db from '@/db';

Admin.init({
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    unique: true
  },
  login: {
    type: Sequelize.STRING(64),
    allowNull: false,
    unique: true
  },
  password: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  salt: {
    type: Sequelize.STRING,
    allowNull: false,
  }
}, {
  sequelize: db.sq,
  modelName: 'admin',
  timestamps: false
});

// db.checkConnect().then(() => {});
export default Admin