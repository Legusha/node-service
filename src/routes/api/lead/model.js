import Sequelize from 'sequelize'
class Leads extends Sequelize.Model {}
import db from '@/db';

Leads.init({
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    unique: true
  },
  name: {
    type: Sequelize.STRING(256)
  },
  email: {
    type: Sequelize.STRING(256),
    allowNull: false,
    unique: true
  }
}, {
  sequelize: db.sq,
  modelName: 'leads',
  timestamps: false
});

export default Leads