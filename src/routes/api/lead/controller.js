import Leads from './model'

export const index = {
  validateEmail (email, next) {
    const pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (pattern.test(email)) return true;
    next({status: 400, message: 'invalid email'});
    return false;
  },
  async post (req, res, next) {
    const { email, name } = req.body;
    try {
      if (!this.validateEmail(email, next)) {
        return;
      }
      await Leads.sync();
      await Leads.create({
        name,
        email
      });
      res.status(200).json({});
    } catch (e) {
      console.error(e);
      if ('original' in e && e.original.code === 'ER_DUP_ENTRY') {
        next({status: 409, message: 'duplicate_email'});
        return
      }
      next(e);
    }
  }
};
