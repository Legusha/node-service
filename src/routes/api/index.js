import { Router } from 'express'
const router = Router();

import lead from './lead'
import admin from './admin'

router.use('/createLead', lead);
router.use('/admin', admin);

export default router;
