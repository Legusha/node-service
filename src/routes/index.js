import { Router } from 'express'
const router = Router();
// import queryList from '@/db/query-list';

/* GET home page. */
router.get('/', async function(req, res, next) {
  res.render('index', { title: 'Express' });
});

export default router;
