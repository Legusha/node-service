import Sequelize from 'sequelize';

const initDb = function () {
  const sq = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASSWORD, {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: 'mysql'
  });
  return  {
    sq: sq,
    checkConnect: function () {
      return this.sq
        .authenticate()
        .then(() => {
          console.log('Connection has been established successfully.');
          return this.sq
        }).catch(err => {
          console.error('Unable to connect to the database:', err);
      });
    },
    close : function () {
      return this.sq
        .close()
        .then(() => {
          console.log('Connection close');
        }).catch(err => {
          console.error('Error disconnected from database:', err);
        });
    }
  }
};

export default initDb();